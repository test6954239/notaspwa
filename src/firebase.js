
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getMessaging, getToken } from 'firebase/messaging'

const firebaseConfig = {
  apiKey: "AIzaSyBR1_HRTIsl1NwCYShErEFu1US0jJBIq4k",
  authDomain: "test-push-83be6.firebaseapp.com",
  projectId: "test-push-83be6",
  storageBucket: "test-push-83be6.appspot.com",
  messagingSenderId: "595623197887",
  appId: "1:595623197887:web:4c17fc37c0e9ccfa73da0a",
  measurementId: "G-0JKRW1DMC4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const messaging = getMessaging(app);