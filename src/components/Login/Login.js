import React from 'react';
import './Login.css'; 
import {getAuth, signInAnonymously } from 'firebase/auth'
import { Link } from 'react-router-dom';
import { Button } from 'antd';

import portada from '../../assets/login.png'


const Login = () => {
    const login = () => {
        signInAnonymously(getAuth()).then((usuario) => console.log(usuario))
    }
  return (
    <div className="login-container">
      <img
        src={portada}
        alt="Imagen de inicio de sesión"
        className="login-image"
      />
      <Button  type="primary" onClick={login}>

            <Link to="/notes">Iniciar sesión</Link>

      </Button>
      
    </div>
  );
};

export default Login;


