const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
	_id: String, // Modificamos el tipo de dato del campo _id
	title: String,
	description: String,
	dueDate: Date,
	priority: String,
	estado: String,
});

module.exports = mongoose.model('Note', noteSchema);
