//sw-db.js
// Utilidades para grabar PouchDB

const db = new PouchDB('mensajes', {
	skip_setup: true,
	ajax: { withCredentials: false },
});

function guardarMensaje(mensaje) {
	console.log('Guardando mensaje en PouchDB:', mensaje);
	// Generar un nuevo _id compatible con MongoDB ObjectId
	//mensaje._id = new PouchDB.utils.uuid(32, 16).toLowerCase();
	//mensaje._id = new Date().toISOString().toLowerCase();
	mensaje._id = new Date().toISOString();

	return db
		.put(mensaje)
		.then(() => {
			self.registration.sync.register('nuevo-post');
			const newResp = { ok: true, offline: true };

			// Mostrar mensaje en la consola
			console.log('Mensaje guardado con éxito');

			return new Response(JSON.stringify(newResp));
		})
		.catch((error) => {
			console.error('Error al guardar mensaje en PouchDB:', error);
			throw error;
		});
}

function postearMensajes() {
	console.log('Iniciando sincronización...');
	const posteos = [];
	return db
		.allDocs({ include_docs: true })
		.then((docs) => {
			docs.rows.forEach((row) => {
				const doc = row.doc;

				const fetchPom = fetch('http://localhost:3000/api/note', {
					method: 'POST',
					mode: 'cors', // Agrega esto para especificar el modo cors
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(doc),
				})
					.then((res) => {
						if (!res.ok) {
							// Manejar el error, por ejemplo, podrías intentar volver a sincronizar más tarde
							console.error(
								'Error al sincronizar mensaje:',
								res.statusText
							);
						}
						return db.remove(doc);
					})
					.catch((error) => {
						console.error('Error al obtener mensajes de PouchDB:', error);
						throw error;
					});

				posteos.push(fetchPom);
			});

			// Devolver la promesa para que el evento de sincronización espere a que se completen todas las operaciones
			return Promise.all(posteos).catch((error) => {
				console.error('Error al sincronizar mensajes:', error);
				throw error; // Propagar el error para que puedas verlo en la consola del navegador
			});
		})
		.catch((error) => {
			console.error('Error al obtener documentos de PouchDB:', error);
			throw error;
		});
}
