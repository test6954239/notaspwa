importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js');
  


const firebaseConfig = {
  apiKey: "AIzaSyBR1_HRTIsl1NwCYShErEFu1US0jJBIq4k",
  authDomain: "test-push-83be6.firebaseapp.com",
  projectId: "test-push-83be6",
  storageBucket: "test-push-83be6.appspot.com",
  messagingSenderId: "595623197887",
  appId: "1:595623197887:web:4c17fc37c0e9ccfa73da0a",
  measurementId: "G-0JKRW1DMC4"
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage(payload => {
    console.log('Recibiendo mensaje en segundo plano')
    const tituloNotificacion = payload.notificacion.title;
    const options = {
        body: payload.notification.body,
        icon: '../img/logoNotas.png',
    }

    self.registration.showNotification(tituloNotificacion, options)
})